package com.zendor.foodmonk.foodmonk.rest;

import com.zendor.foodmonk.foodmonk.model.User;

import org.json.JSONArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
public interface ApiInterface {
    @PUT("movie/top_rated")
    Call<User> login(@Query("email") String email,@Query("password") String password);

    @GET("/site/{id}/activemenu")
    Call<List<Object>> getActiveMenu(@Path("id") String id);
}
