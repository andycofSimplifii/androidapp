package com.zendor.foodmonk.foodmonk.rest;

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "http://alpha.foodmonk.com/api/v1/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
