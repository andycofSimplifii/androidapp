package com.zendor.foodmonk.foodmonk;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public class MenuCategoryAdapter extends ExpandableRecyclerAdapter<MenuCategoryViewHolder, MenuViewHolder> {

    private LayoutInflater mInflator;

    public MenuCategoryAdapter(Context context, List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        mInflator = LayoutInflater.from(context);
    }

    @Override
    public MenuCategoryViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View MenuCategoryView = mInflator.inflate(R.layout.menu_category_view, parentViewGroup, false);
        return new MenuCategoryViewHolder(MenuCategoryView);
    }

    @Override
    public MenuViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View MenuView = mInflator.inflate(R.layout.menu_view, childViewGroup, false);
        return new MenuViewHolder(MenuView);
    }

    @Override
    public void onBindParentViewHolder(MenuCategoryViewHolder MenuCategoryViewHolder, int position, ParentListItem parentListItem) {
        MenuCategory MenuCategory = (MenuCategory) parentListItem;
        MenuCategoryViewHolder.bind(MenuCategory);
    }

    @Override
    public void onBindChildViewHolder(MenuViewHolder MenuViewHolder, int position, Object childListItem) {
        Menu Menu = (Menu) childListItem;
        MenuViewHolder.bind(Menu);
    }
}
