package com.zendor.foodmonk.foodmonk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class MenuActivity extends AppCompatActivity{

    private MenuCategoryAdapter mAdapter;
    private RecyclerView recyclerView;
    private RequestQueue mRequestQueue;
    private StringRequest mStringRequest;
    JSONArray  jsonArray;
    private String url = "http://alpha.foodmonk.com/api/v1/site/5b3267a85e812a530e9359a0/activemenu";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        SharedPreferences.Editor editor = getSharedPreferences("TotalSum", MODE_PRIVATE).edit();
        editor.putInt("sum", 0);
        editor.apply();
        JSONArray ja=sendAndRequestResponse();
        Log.d("value",ja.toString());
        Menu  Menu_one = new Menu("The Shawshank Redemption");
        Menu  Menu_two  = new Menu("The Godfather");
        Menu  Menu_three = new Menu("The Dark Knight");
        Menu Menu_four  = new Menu("Schindler's List ");
        Menu Menu_five = new Menu("12 Angry Men ");
        Menu Menu_six = new Menu("Pulp Fiction");
        Menu Menu_seven = new Menu("The Lord of the Rings: The Return of the King");
        Menu Menu_eight = new Menu("The Good, the Bad and the Ugly");
        Menu Menu_nine = new Menu("Fight Club");
        Menu Menu_ten = new Menu("Star Wars: Episode V - The Empire Strikes");
        Menu Menu_eleven = new Menu("Forrest Gump");
        Menu Menu_tweleve = new Menu("Inception");

        MenuCategory molvie_category_one = new MenuCategory("Drama", Arrays.asList(Menu_one, Menu_two, Menu_three, Menu_four));
        MenuCategory molvie_category_two = new MenuCategory("Action", Arrays.asList(Menu_five, Menu_six, Menu_seven,Menu_eight));
        MenuCategory molvie_category_three = new MenuCategory("History", Arrays.asList(Menu_nine, Menu_ten, Menu_eleven,Menu_tweleve));
        MenuCategory molvie_category_four = new MenuCategory("Thriller", Arrays.asList(Menu_one, Menu_five, Menu_nine,Menu_tweleve));

        final List<MenuCategory> MenuCategories = Arrays.asList(molvie_category_one,  molvie_category_two, molvie_category_three,molvie_category_four);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new MenuCategoryAdapter(this, MenuCategories);
        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                MenuCategory expandedMenuCategory = MenuCategories.get(position);

                String toastMsg = getResources().getString(R.string.expanded, expandedMenuCategory.getName());
                Toast.makeText(MenuActivity.this,
                        toastMsg,
                        Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onListItemCollapsed(int position) {
                MenuCategory collapsedMenuCategory = MenuCategories.get(position);

                String toastMsg = getResources().getString(R.string.collapsed, collapsedMenuCategory.getName());
                Toast.makeText(MenuActivity.this,
                        toastMsg,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mAdapter.onRestoreInstanceState(savedInstanceState);
    }
    public void sum()
    {
        int idName=0;
        SharedPreferences prefs = getSharedPreferences("TotalSum", MODE_PRIVATE);
        String restoredText = prefs.getString("text", null);
        if (restoredText != null) {
           // String name = prefs.getString("name", "No name defined");//"No name defined" is the default value.
            idName = prefs.getInt("sum", 0); //0 is the default value.
        }
        idName+=200;
        SharedPreferences.Editor editor = getSharedPreferences("TotalSum", MODE_PRIVATE).edit();
        editor.putInt("sum", idName);
        editor.apply();
    }
    public void summary(View view) {
        Intent Intent = new Intent(this, Summary.class);
        startActivity(Intent);
    }
    private JSONArray sendAndRequestResponse() {

        //RequestQueue initialized
        mRequestQueue = Volley.newRequestQueue(this);

        //String Request initialized
        mStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(getApplicationContext(),"Response :" + response.toString(), Toast.LENGTH_LONG).show();//display the response on screen
                try {
                     jsonArray = new JSONArray(response.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Log.i("Error :" + error.toString());
            }
        });

        mRequestQueue.add(mStringRequest);
        return jsonArray;
    }
}
