package com.zendor.foodmonk.foodmonk;

import java.util.List;

public class MenuCategory  implements ParentListItem{
    private String mName;
    private List<Menu> mMovies;

    public MenuCategory(String name, List<Menu> movies) {
        mName = name;
        mMovies = movies;
    }

    public String getName() {
        return mName;
    }
    @Override
    public List<?> getChildItemList() {
        return mMovies;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

}
