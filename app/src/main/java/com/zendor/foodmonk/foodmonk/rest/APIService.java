package com.zendor.foodmonk.foodmonk.rest;


import com.zendor.foodmonk.foodmonk.model.Login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface APIService {

    @PUT("/posts")
    @FormUrlEncoded
    Call<Login> login(@Field("email") String email,
                         @Field("password") String password);
}
