package com.zendor.foodmonk.foodmonk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zendor.foodmonk.foodmonk.rest.APIService;
import com.zendor.foodmonk.foodmonk.rest.ApiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.PUT;

public class LoginActivity2 extends AppCompatActivity {


    private JSONObject jsonObj;
    private APIService mAPIService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);


    }

    public void onClickBtn(View v) {
        Boolean status = null;
        EditText email = findViewById(R.id.l1);
        EditText password = findViewById(R.id.l2);
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();
        mAPIService = ApiUtils.getAPIService();
        JSONObject js = null;
        try {
            js = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            status = js.getBoolean("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (status = true) {
            Intent i = new Intent(getApplicationContext(), Navigation.class);
            startActivity(i);

        }

    }

    public void loginuser(String email, String password) {
        mAPIService.login(email, password).enqueue(new Callback<PUT>() {
            @Override
            public void onResponse(Call<PUT> call, Response<PUT> response) {

                if(response.isSuccessful()) {
                    showResponse(response.body().toString());
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<PUT> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }
}