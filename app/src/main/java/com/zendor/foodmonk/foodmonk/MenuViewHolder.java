package com.zendor.foodmonk.foodmonk;

import android.view.View;
import android.widget.TextView;

public class MenuViewHolder extends ChildViewHolder {

    private TextView mMoviesTextView;

    public MenuViewHolder(View itemView) {
        super(itemView);
        mMoviesTextView = (TextView) itemView.findViewById(R.id.tv_movies);
    }

    public void bind(Menu menu) {
        mMoviesTextView.setText(menu.getName());
    }
}
